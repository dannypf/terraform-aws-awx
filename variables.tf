variable hostname {
  type = string
  default = "awx"
}

variable key_name {
  type    = string
  default = "go"
}

variable tags {
  type = map
  default = {
    TTL   = "48"
    owner = "Grant Orchard"
  }
}

variable instance_type {
  type    = string
  default = "t2.medium"
}

variable instance_profile_path {
  description = "Path in which to create the IAM instance profile."
  default     = "/"
}

variable remote_tfc_organization {
	default = "grantorchard"
}

variable remote_tfc_workspace {
	default = "terraform-aws-core"
}