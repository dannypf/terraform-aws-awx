data terraform_remote_state "this" {
  backend = "remote"

  config = {
    organization = var.remote_tfc_organization
    workspaces = {
      name = var.remote_tfc_workspace
    }
  }
}

locals {
  public_subnets = data.terraform_remote_state.this.outputs.public_subnets
  security_group_outbound = data.terraform_remote_state.this.outputs.security_group_outbound
  security_group_ssh = data.terraform_remote_state.this.outputs.security_group_ssh
  vpc_id = data.terraform_remote_state.this.outputs.vpc_id
}

data aws_route53_zone "this" {
  name         = "go.hashidemos.io"
  private_zone = false
}

data aws_ami "ubuntu" {
  most_recent = true

  filter {
    name = "tag:application"
    values = ["awx"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["711129375688"] # HashiCorp account
}

module "awx" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.13.0"

  name = var.hostname
  instance_count = 1

  ami = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name = var.key_name
  iam_instance_profile = aws_iam_instance_profile.this.name

  monitoring = true
  vpc_security_group_ids = [
    local.security_group_outbound,
    local.security_group_ssh,
    module.security_group_http.this_security_group_id
  ]
  #user_data_base64 = base64gzip(data.template_file.userdata_postgresql[0].rendered)
  subnet_id = local.public_subnets[0]
  tags = var.tags
}

resource aws_route53_record "this" {
  zone_id = data.aws_route53_zone.this.id
  name    = "${var.hostname}.${data.aws_route53_zone.this.name}"
  type    = "A"
  ttl     = "300"
  records = [module.awx.public_ip[0]]
}

module "security_group_http" {
  source  = "terraform-aws-modules/security-group/aws"

  name        = "awx-http"
  description = "http access"
  vpc_id      = local.vpc_id

  ingress_cidr_blocks = [
    "220.235.156.186/32",
    "10.0.0.0/16"
    ]
  ingress_rules = ["http-80-tcp"]
  tags = var.tags
}

data aws_iam_policy_document "assume" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data aws_iam_policy_document "discover_tags" {
  statement {
    actions = [
      "ec2:DescribeInstances",
      "ec2:DescribeTags",
      "autoscaling:DescribeAutoScalingGroups"
    ]

    resources = ["*"]
  }
}


resource aws_iam_instance_profile "this" {
  name_prefix = var.hostname
  path        = var.instance_profile_path
  role        = aws_iam_role.this.name
}

resource aws_iam_role "this" {
  name_prefix        = var.hostname
  assume_role_policy = data.aws_iam_policy_document.assume.json
}

resource aws_iam_role_policy "this" {
  name   = var.hostname
  role   = aws_iam_role.this.id
  policy = data.aws_iam_policy_document.discover_tags.json
}

# aws ec2 describe-instances --filters Name="tag:owner",Values="Grant Orchard"

module "test" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "2.13.0"

  name = "test"
  instance_count = 1

  ami = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name = var.key_name

  monitoring = true
  vpc_security_group_ids = [
    local.security_group_outbound,
    local.security_group_ssh
  ]
  #user_data_base64 = base64gzip(data.template_file.userdata_postgresql[0].rendered)
  subnet_id = local.public_subnets[0]
  tags = merge(var.tags, {"vault_agent" = true})
}





